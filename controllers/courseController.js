const Course = require('../models/Course')
const auth = require('../auth')

module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price 
	})

	return newCourse.save().then((course,error) => {
		if(error){

			return false

		}else{

			return true
		}
	})
}



//Retrieve all courses

module.exports.getAllCourses = async (data) => {

		if(data.isAdmin){
			return Course.find({}).then(result => {
				return result
			})
		}else{
			return false 
		}
}



module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
}

module.exports.getCourse = (reqParams) =>{
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}


module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		console.log(course)
		if(error){
			return false
		}else{
			return true
		}
	})
}



module.exports.courseArchive = (reqParams, reqBody) => {
	let activeUser = {
		isActive: reqBody.isActive
	}

	return Course.findById(reqParams.courseId, activeUser).then((result,error)=>{
		if(error){
			return false
		}else{
			return true
		}
	})

}
	



	
